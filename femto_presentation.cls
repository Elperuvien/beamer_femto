\ProvidesClass{femto_presentation}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions\relax
\LoadClass[9pt, aspectratio=169]{beamer}
\RequirePackage{lmodern}

\RequirePackage{babel}
\RequirePackage[T1]{fontenc} 
\RequirePackage[utf8]{inputenc} 

\RequirePackage[default]{lato}

% ------------------------------------------------------ Définition de couleurs
\definecolor{bf}{HTML}{63277E} % bleu foncé
\definecolor{bl}{HTML}{018FCD} % bleu clair
\definecolor{v}{HTML}{9FD05A}  % vert clair femto
\definecolor{readablegreen}{RGB}{76,133,14}% vert foncé
\definecolor{g}{HTML}{CCCCCD}  % gris
\definecolor{gris_fonce}{HTML}{9d9ea0}
\definecolor{gris_clair}{HTML}{e9ebea}
\definecolor{redfemto}{RGB}{192,0,0}
\definecolor{cerise}{HTML}{BB0B0B}

%%%%%%%%%%%%%%%%%%%%%%%%%     Def. footer     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


% ----------------------------------------------------------------------- Thème
\usetheme{CambridgeUS} % split, Malmoe, Copenhagen, CambridgeUS
%% Utiliser le theme externe d un autre thème
%\useoutertheme{infolines}
%% Utiliser les couleurs d un autre thème
\usecolortheme[named=bf]{structure}

% ------ Modification manuelle des couleurs des éléments beamer

% Titre
\setbeamercolor{title}{fg=bf,bg=gris_clair}
\setbeamerfont{title}{size={\fontsize{14pt}{16pt}}}
\setbeamerfont{author}{size={\fontsize{8pt}{10pt}}}
\setbeamerfont{date}{size={\fontsize{8pt}{10pt}}}

%\setbeamercolor{titlelike}{shading=true}

% Fond
\setbeamercolor{background canvas}{bg=white}

% Page de sommaire / Table of contents (toc)
\setbeamercolor{section in toc}{fg=bf}
\setbeamercolor{subsection in toc}{fg=bf}
\setbeamercolor{section number projected}{bg=bf,fg=white}
\setbeamertemplate{section in toc}[circle] % ball
\setbeamertemplate{subsection in toc}
{\leavevmode\leftskip=1em$\bullet$\hskip0.5em\inserttocsubsection\par}

% Sommaire en haut de slide (Header)
\setbeamercolor{frametitle}{fg=bf,bg=white}
% font 14pt, skipline size 16pt
\setbeamerfont{frametitle}{size={\fontsize{14pt}{16pt}}} 
\setbeamercolor{section in head/foot}{fg = bf, bg = white}
\setbeamercolor{subsection in head/foot}{fg = bf, bg = white}
\setbeamertemplate{headline}
{%
	\vskip2pt%
	\insertsectionnavigationhorizontal{.71\textwidth}{}{\hskip0pt plus1filll}
	\insertsubsectionnavigationhorizontal{.71\textwidth}{}{\hskip0pt plus1filll}
	\vskip6pt
}

% Pieds de page (Footer)
\setbeamercolor{author in head/foot}{fg=white,bg=bf}
\setbeamercolor{title in head/foot}{fg=bf,bg=gris_clair}
\setbeamercolor{date in head/foot}{fg=white,bg=bf}

% Items
\setbeamercolor{item projected}{bg=bf} % couleur des puces
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{itemize subitem}[square]
\setbeamertemplate{itemize subsubitem}[triangle]

% Enumerations
\setbeamercolor{enumerate projected}{bg=bf} % couleur des puces
\setbeamertemplate{enumerate items}[circle]
\setbeamertemplate{enumerate subitem}[square]
\setbeamertemplate{enumerate subsubitem}[triangle]

% Blocks
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamercolor{block title}{fg=bf, bg=gris_clair}
\setbeamercolor{block body}{fg=black, bg=white}

% Alert Block
\setbeamercolor{block title alerted}{fg=white, bg=redfemto}
\setbeamercolor{block body alerted}{fg=redfemto, bg=white}

% Example Block
\setbeamercolor{example text}{fg=green!50!black,bg=gris_clair}
\setbeamercolor{block body example}{fg=black, bg=white}

% ------------------------------------------------------- Mise en page
%% Avoir trois box en pieds de page (auteur - titre - date et n° de page)
\defbeamertemplate*{footline}{split slidenumber right}
{%
		\begin{columns}
				\begin{column}{0.1\paperwidth}
						\centering
						\includegraphics[width=.9\textwidth]{logo-FEMTO}
				\end{column}
				\hfill
				\begin{column}{0.7\paperwidth}
						\begin{beamercolorbox}[
								wd=\textwidth,
								ht=3ex,
								dp=1ex,
								center]{title in head/foot}%
								\insertshorttitle{} 
						\end{beamercolorbox}%

						\begin{beamercolorbox}[
								wd=\textwidth,
								ht=3ex,
								dp=1ex,
								center]{title in head/foot}%
								\hfill \insertshortinstitute{} \hfill | \hfill
								\insertshortauthor \hfill | \hfill
								\insertshortdate \hfill
						\end{beamercolorbox}%
				\end{column}
				\hfill
				\begin{column}{0.1\paperwidth}
						\begin{beamercolorbox}[
								wd=\textwidth,
								ht=3ex,
								dp=1ex,
								right]{date in head/foot}%
								\insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
						\end{beamercolorbox}
				\end{column}
		\end{columns}
}%

\setbeamerfont{footline}{size=\fontsize{6}{8}\selectfont}
\setbeamerfont{headline}{size=\fontsize{6}{8}\selectfont}

%% Ajuster la largeur des marges latérales des diapos
%\setbeamersize{text margin left=10pt,text margin right=10pt}

%% Modifier les bas de page pour avoir une parenthèse autour du renvoi
\renewcommand{\thefootnote}{\texttt{(\arabic{footnote})}}


%% Changer la numérotation des slides 
%% (nb total pris avant les annexes et annexes ajoutées avec par exemple 51/50)
% Insérer \backupbegin avant les annexes et backupend après les annexes
\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}


%%%%%%%%%%%%%%%%%%%%%%%%%%% Modification des planches %%%%%%%%%%%%%%%%%%%%%%%%%

%% Modifier interligne
\RequirePackage{setspace}

%% Afficher les éléments d une liste sur plusieurs colonnes :
\RequirePackage{multicol}


%% Changer localement la largeur de la page (pour une grande figure par exemple)
\RequirePackage{changepage}
% A utiliser avec
%\begin{figure}
%\begin{adjustwidth}{-1cm}{-1cm}
%[insert object here] \end{adjustwidth}
%\end{figure}


%- -----------------------------------------------------Packages pour les maths
%% Operateurs mathématiques (tels que \sin)
\RequirePackage{amsmath}
%% Symboles mathématiques (tels que les ensembles ou lettres gothiques)
\RequirePackage{amsfonts}
%% Autre ensemble de symboles mathématiques (charge automatiquement amsfonts)
\RequirePackage{amssymb}
%% Structure de type théorème
%\RequirePackage{amsthm}

% --------------------------------------------------- Packages pour les figures
\RequirePackage{graphicx}
%% Chemin des figures
\graphicspath{{Images/}, {fig/}, {graphics/}}
%% Diviser la figures en plusieurs sous-figures (a), (b), ...
\RequirePackage{subfigure}
%% Mettre des labels aux subfigures
%\RequirePackage{caption}
%\RequirePackage{subcaption}
%\RequirePackage{cleveref}

%% Insérer des animations ou des gif
\RequirePackage{animate}

%% Package pour dessiner des flèches
\RequirePackage{tikz}
    \usetikzlibrary{shadows,trees}
    \usetikzlibrary{shapes.arrows}
    \tikzset{no shadows/.style={general shadow/.style=}}

% -------------------------------------------------- Packages pour les tableaux
\RequirePackage{multirow}
\RequirePackage{tabularx}
\RequirePackage{array,multirow,makecell}

%% Hauteur des cellules du tableau
\setcellgapes{4pt}
\makegapedcells

%% Redéfinition de la position du contenu des cellules
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

%% Packages pour ajuster la taille du tableau à la largeur de la page
\RequirePackage{adjustbox}

% --------------------------------------------------------------- Page de titre
%\RequirePackage{enumitem}
%\frenchbsetup{StandardLists=true}
%\setbeamercovered{transparent} 

\setbeamertemplate{navigation symbols}{} 

%\institute{} 
%\subject{} 

%% Inclusion de liens internet
\RequirePackage{url}

%% Inclusion de pdf tel quel, penser à préciser le nombre de page
%\RequirePackage{pdfpages} % commande : \includepdf[pages = {1-n}]{nom.pdf}

% Barrer et hachurer du texte
\RequirePackage{ulem}
%\sout{mon texte barré une seule fois}
%\xout{mon texte hachuré et peu lisible d ailleurs}
\RequirePackage{cancel}
%\cancel draws a diagonal line (slash) through its argument.
%\bcancel uses the negative slope (a backslash).
%\xcancel draws an X (actually \cancel plus \bcancel).
%\cancelto{?value?}{?expression?} draws a diagonal arrow through the ?expression?, pointing to the ?value?


% Ajout du logo Femto-ST

\addtobeamertemplate{frametitle}{\vspace*{0cm}}{%
\begin{tikzpicture}[
	every node/.style={no shadows/.style={general shadow/.style=}},
	remember picture,
	overlay]
\node[
	anchor=north east,
	yshift=2.5pt,
	xshift=2.5pt] at (current page.north east){\includegraphics[height=1.5cm]{motif_1}};
\end{tikzpicture}

\vspace*{-.7cm}
}
